package com.sinothk.android.views.fileView;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.sinothk.android.views.R;
import com.sinothk.android.views.fileView.bean.FileShowListEntity;

import java.util.List;

/**
 * <pre>
 *  @author 梁玉涛 (https://github.com/sinothk)
 *  @Create 2018/2/10 10:01
 *  @Project: UIPluginLib
 *  @Description: 商城中，产品详情介绍
 *  @Update:
 * <pre>
 */
public class FileLocListView extends LinearLayout {

    public FileLocListView(Context context) {
        super(context);
    }

    public FileLocListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public FileLocListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FileLocListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setData(final List<FileShowListEntity> subList) { // , final OnItemClickListener listener

        this.removeAllViews();
        this.removeAllViewsInLayout();

        if (subList == null || subList.size() == 0) {
            return;
        }
        for (int i = 0; i < subList.size(); i++) {
            final FileShowListEntity data = subList.get(i);

            View view = LayoutInflater.from(getContext()).inflate(R.layout.file_loc_list_view, null);

            RelativeLayout itemView = view.findViewById(R.id.itemView);
            TextView fileNameTv = view.findViewById(R.id.fileNameTv);
            TextView delBtn = view.findViewById(R.id.delBtn);

            fileNameTv.setText(data.getFileName() != null ? data.getFileName() : "未知文件");

            final int finalI = i;
            delBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        // 删除按钮
                        itemClickListener.onFileListItemClick(1, finalI, data);
                    }
                }
            });

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        // 条目点击
                        itemClickListener.onFileListItemClick(0, finalI, data);
                    }
                }
            });

            this.addView(view, i);
        }
    }

    private OnItemClickListener itemClickListener;

    public void setFileListItemClick(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        void onFileListItemClick(int eventType, int position, FileShowListEntity entity);
    }
}
