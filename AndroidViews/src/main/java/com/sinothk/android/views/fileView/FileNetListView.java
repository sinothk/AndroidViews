package com.sinothk.android.views.fileView;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.sinothk.android.utils.XUtils;
import com.sinothk.android.views.R;
import com.sinothk.android.views.fileView.bean.FileShowListEntity;

import java.text.DecimalFormat;
import java.util.List;

/**
 * <pre>
 *  @author 梁玉涛 (https://github.com/sinothk)
 *  @Create 2018/2/10 10:01
 *  @Project: UIPluginLib
 *  @Description: 商城中，产品详情介绍
 *  @Update:
 * <pre>
 */
public class FileNetListView extends LinearLayout {

    public FileNetListView(Context context) {
        super(context);
    }

    public FileNetListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public FileNetListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FileNetListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setData(final List<FileShowListEntity> subList, final OnItemClickListener listener) {

        this.removeAllViews();

        if (subList == null || subList.size() == 0) {
            return;
        }
        for (int i = 0; i < subList.size(); i++) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.file_net_list_view, null);

            TextView fileNameTv = view.findViewById(R.id.fileNameTv);
            fileNameTv.setText(XUtils.string().getNotNullValue(subList.get(i).getFileName(), "未知文件"));

            try {
                TextView fileSizeTv = view.findViewById(R.id.fileSizeTv);
                fileSizeTv.setText(getFileSize(subList.get(i).getFileSize()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            RelativeLayout itemView = view.findViewById(R.id.itemView);

            final int finalI = i;

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(subList.get(finalI));
                }
            });

            this.addView(view, i);
        }

    }

    public interface OnItemClickListener {
        void onItemClick(FileShowListEntity entity);
    }

    public String getFileSize(String sizeStr) {

        if (sizeStr == null) {
            return "0";
        }

        double size = Double.valueOf(sizeStr);

        //获取到的size为：1705230
        long TB = 1024 * 1024 * 1024 * 1024L;//定义TB的计算常量
        int GB = 1024 * 1024 * 1024;//定义GB的计算常量
        int MB = 1024 * 1024;//定义MB的计算常量
        int KB = 1024;//定义KB的计算常量

        DecimalFormat df = new DecimalFormat("0.0");//格式化小数
        String resultSize;

        if (size / TB >= 1) {
            //如果当前Byte的值大于等于1GB
            resultSize = df.format(size / (float) TB) + "TB";

        } else if (size / GB >= 1) {
            //如果当前Byte的值大于等于1GB
            resultSize = df.format(size / (float) GB) + "GB";
        } else if (size / MB >= 1) {
            //如果当前Byte的值大于等于1MB
            resultSize = df.format(size / (float) MB) + "MB";
        } else if (size / KB >= 1) {
            //如果当前Byte的值大于等于1KB
            resultSize = df.format(size / (float) KB) + "KB";
        } else {
            resultSize = size + "B";
        }
        return resultSize;
    }
}
