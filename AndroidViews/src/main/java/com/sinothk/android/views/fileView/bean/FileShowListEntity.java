package com.sinothk.android.views.fileView.bean;

/**
 * <pre>
 *  创建:  梁玉涛 2019/8/12 on 15:53
 *  项目: ZySjtlAndroid
 *  描述:
 *  更新:
 * <pre>
 */
public class FileShowListEntity {

    private String fileId;

    private String fileOldName;
    private String fileName;
    private String filePath;
    private String url;
    private String fileSize;
    private boolean isNetData; // 1: 本地；2. 网络

    public FileShowListEntity() {
    }

    public FileShowListEntity(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public FileShowListEntity(String fileId, String fileName, String filePath) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isNetData() {
        return isNetData;
    }

    public void setNetData(boolean netData) {
        isNetData = netData;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileOldName() {
        return fileOldName;
    }

    public void setFileOldName(String fileOldName) {
        this.fileOldName = fileOldName;
    }
}
