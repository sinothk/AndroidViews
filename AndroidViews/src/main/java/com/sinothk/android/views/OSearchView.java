package com.sinothk.android.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;

/*
    <cn.android.x.view.OSearchView
        android:id="@+id/oSearchView"
        android:layout_width="match_parent"
        android:layout_height="@dimen/dp_34"
        android:layout_alignParentStart="true"
        android:layout_centerVertical="true"
        android:layout_gravity="center"
        android:layout_marginStart="@dimen/dp_16"
        android:layout_toStartOf="@id/cancelView"
        android:background="@drawable/shape_bg_for_search_view"
        android:gravity="center_vertical"
        android:queryHint="请输入案件关键字"
        app:searchIconRes="@drawable/icon_search_gray"
        app:searchTextColor="@color/red"
        app:searchTextHintColor="@color/page_sub"
        app:searchTextIsBold="true"
        app:searchTextSize="@dimen/sp_14" />
    */

public class OSearchView extends SearchView {
    // 文本部分
    private int searchTextSize = 14; // 字体大小
    private int searchTxtColor = -1; // 字体颜色 OK
    private int searchTextHintColor = -1; // 提示颜色
    private boolean searchTextIsBold = true; //
    private String searchQueryHint = "请输入搜索关键字"; //
    // 图标部分
    private int searchIconRes = -1;//R.drawable.icon_search_blue; // 搜索图标

    public OSearchView(Context context) {
        this(context, null);
    }

    public OSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.OSearchView);
            try {
                searchTxtColor = array.getColor(R.styleable.OSearchView_searchTextColor,
                        ContextCompat.getColor(context, R.color.colorPrimary));
                searchTextHintColor = array.getColor(R.styleable.OSearchView_searchTextHintColor,
                        ContextCompat.getColor(context, R.color.page_sub));
                searchTextIsBold = array.getBoolean(R.styleable.OSearchView_searchTextIsBold, true);
                searchTextSize = array.getDimensionPixelSize(R.styleable.OSearchView_searchTextSize, R.dimen.sp_14);
                searchIconRes = array.getResourceId(R.styleable.OSearchView_searchIconRes, R.drawable.icon_search_gray);
                searchQueryHint = array.getString(R.styleable.OSearchView_searchQueryHint);
            } finally {
                array.recycle();
            }
        } else {
            searchTxtColor = ContextCompat.getColor(context, R.color.colorPrimary);
            searchTextHintColor = ContextCompat.getColor(context, R.color.page_sub);
            searchTextIsBold = true;
            searchTextSize = R.dimen.sp_14;
            searchIconRes = R.drawable.icon_search_gray;
            searchQueryHint = "请输入搜索关键字";
        }

        initView();
    }

    private void initView() {
        this.setQueryHint(searchQueryHint);
        this.onActionViewExpanded();
        this.clearFocus(); //取消聚焦
        this.setIconifiedByDefault(false); //将icon固定在左侧

        //找到searchView文本的控件
        TextView searchTxt = this.findViewById(androidx.appcompat.R.id.search_src_text);
        searchTxt.getPaint().setTextSize(searchTextSize);
        searchTxt.setTextColor(searchTxtColor); //设置字体颜色 Color.parseColor("#FF0000")
        searchTxt.setHintTextColor(searchTextHintColor); //设置提示字体颜色

        searchTxt.setTypeface(searchTextIsBold ? Typeface.defaultFromStyle(Typeface.BOLD) : Typeface.defaultFromStyle(Typeface.NORMAL));

        //去掉searchview下划线
        View view = this.findViewById(androidx.appcompat.R.id.search_plate);
        view.setBackgroundColor(Color.TRANSPARENT);

        // 图标
        ImageView searchIcon = this.findViewById(androidx.appcompat.R.id.search_mag_icon);
        searchIcon.setImageResource(searchIconRes);
        int padding = getResources().getDimensionPixelSize(R.dimen.dp_7);
        searchIcon.setPadding(0, padding, 0, padding);//设置图标和文字跟四周距离
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(searchIcon.getLayoutParams());
        lp.setMargins(0, 0, 0, 0);
        searchIcon.setLayoutParams(lp);
    }

    public void setSearchIconRes(int searchIconRes) {
        this.searchIconRes = searchIconRes;
    }

    public void setSearchTextIsBold(boolean searchTextIsBold) {
        this.searchTextIsBold = searchTextIsBold;
    }

    public void setSearchTextSize(int searchTextSize) {
        this.searchTextSize = searchTextSize;
    }

    public void setSearchTxtColor(int searchTxtColor) {
        this.searchTxtColor = searchTxtColor;
    }

    public void setSearchTextHintColor(int searchTextHintColor) {
        this.searchTextHintColor = searchTextHintColor;
    }

    /**
     * 作为子组件时，调用
     */
    public void show() {
        initView();
    }

    public void setSearchQueryHint(String searchQueryHint) {
        this.searchQueryHint = searchQueryHint;
    }

//    <com.sinothk.android.views.OSearchView
//    android:id="@+id/oSearchView"
//    android:layout_width="match_parent"
//    android:layout_height="@dimen/dp_34"
//    android:layout_alignParentStart="true"
//    android:layout_centerVertical="true"
//    android:layout_gravity="center"
//    android:layout_marginStart="@dimen/dp_16"
//    android:layout_toStartOf="@id/cancelView"
//    android:background="@drawable/shape_bg_for_search_view"
//    android:gravity="center_vertical"
//    android:queryHint="请输入案件关键字"
//    app:searchIconRes="@drawable/icon_search_gray"
//    app:searchTextColor="@color/red"
//    app:searchTextHintColor="@color/page_sub"
//    app:searchTextIsBold="true"
//    app:searchTextSize="@dimen/sp_14" />
}
