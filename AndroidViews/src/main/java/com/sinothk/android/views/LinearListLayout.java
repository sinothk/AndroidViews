package com.sinothk.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

public class LinearListLayout extends LinearLayout {


    public LinearListLayout(Context context) {
        super(context);
    }

    public LinearListLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LinearListLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LinearListLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

//    public void setImageUrlArr(List<String> urlArr, String serverUrlPre) {
//        this.removeAllViews();
//        for (int i = 0; i < urlArr.size(); i++) {
//            ImageView imageView = new ImageView(getContext());
//            ViewGroup.LayoutParams imageViewLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            imageView.setLayoutParams(imageViewLayoutParams);
//            imageView.setAdjustViewBounds(true);
//
//            Glide.with(this)
//                    .load(serverUrlPre + urlArr.get(i))
//                    .error(R.drawable.default_image)
//                    .placeholder(R.drawable.default_image)
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .into(imageView);
//            this.addView(imageView, i);
//        }
//    }
}
