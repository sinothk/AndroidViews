package com.sinothk.android.views.inters;

public interface OnListViewItemSuperListener<T> {
    void onClickListener(int position, String flag, T itemData);
}
