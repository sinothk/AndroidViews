package com.sinothk.android.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;

/*<cn.android.x.view.TitleBarSearchView xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:id="@+id/titleBarSearchView"
        android:layout_width="match_parent"
        android:layout_height="@dimen/dp_70"
        app:sub_searchIconRes="@drawable/icon_search_blue"
        app:sub_searchQueryHint="请输入案件关键字"
        app:sub_searchTextColor="@color/theme_color"
        app:sub_searchTextHintColor="@color/page_sub"
        app:sub_searchTextIsBold="true"
        app:sub_searchTextSize="@dimen/sp_14" />

        // 正常使用
        titleBarSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                XUtils.toast().show(query)
                loadingTipView.showLoading("查询中")
                refreshData()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        }).show(this)
 */
public class TitleBarSearchView extends LinearLayout {

    public TitleBarSearchView(Context context) {
        super(context);
        initView(context, null);
    }

    public TitleBarSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public TitleBarSearchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public TitleBarSearchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private View view;
    private LinearLayout rootView;
    // 布局
    private TextView cancelBtn;
    private OSearchView oSearchView;
    private View cancelLine;

    // 属性值
    private boolean cancelEnable = true;
    private OnClickListener cancelClickListener;

    private void initView(Context mContext, AttributeSet attrs) {
        view = LayoutInflater.from(mContext).inflate(R.layout.title_bar_search_view, null);
        rootView = view.findViewById(R.id.rootView);
        // 控件
        cancelBtn = view.findViewById(R.id.cancelBtn);
        cancelLine = view.findViewById(R.id.cancelLine);
        oSearchView = view.findViewById(R.id.oSearchView);
        // 加入父视图
        addView(view);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, rootView.getHeight()));
        setOrientation(VERTICAL);

        // 设置属性
        getValue(mContext, attrs);
    }

    private void getValue(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TitleBarSearchView);
            try {
                searchTxtColor = array.getColor(R.styleable.TitleBarSearchView_sub_searchTextColor,
                        ContextCompat.getColor(context, R.color.colorPrimary));
                searchTextHintColor = array.getColor(R.styleable.TitleBarSearchView_sub_searchTextHintColor,
                        ContextCompat.getColor(context, R.color.page_sub));
                searchTextIsBold = array.getBoolean(R.styleable.TitleBarSearchView_sub_searchTextIsBold, true);

                searchTextSize = array.getDimensionPixelSize(R.styleable.TitleBarSearchView_sub_searchTextSize, R.dimen.sp_14);

                searchIconRes = array.getResourceId(R.styleable.TitleBarSearchView_sub_searchIconRes, R.drawable.icon_search_gray);

                searchQueryHint = array.getString(R.styleable.TitleBarSearchView_sub_searchQueryHint);
            } finally {
                array.recycle();
            }
        } else {
            searchTextSize = R.dimen.sp_14;
            searchTxtColor = ContextCompat.getColor(context, R.color.colorPrimary);
            searchTextHintColor = ContextCompat.getColor(context, R.color.page_sub);
            searchIconRes = R.drawable.icon_search_gray;
            searchTextIsBold = true;
            searchQueryHint = "请输入搜索关键字";
        }


    }

    /**
     * 操作部分 =========================================================================
     */
    public TitleBarSearchView setCancelEnable(boolean cancelEnable) {
        this.cancelEnable = cancelEnable;
        return this;
    }

    public TitleBarSearchView setCancelEnable(OnClickListener cancelClickListener) {
        this.cancelEnable = true;
        this.cancelClickListener = cancelClickListener;
        return this;
    }

    public TitleBarSearchView setOnQueryTextListener(SearchView.OnQueryTextListener onQueryTextListener) {
        this.onQueryTextListener = onQueryTextListener;
        return this;
    }

    public void show(Activity currActivity) {
        // 取消按钮设置
        setCancelView(currActivity);
        // 设置搜索输入框
        setSearchView();

        oSearchView.show();
    }

    private void setSearchView() {
        oSearchView.setSearchIconRes(searchIconRes);
        oSearchView.setSearchTextIsBold(searchTextIsBold);
        oSearchView.setSearchTextSize(searchTextSize);
        oSearchView.setSearchTxtColor(searchTxtColor);
        oSearchView.setSearchTextHintColor(searchTextHintColor);
        //
        oSearchView.setSearchQueryHint(searchQueryHint);// = "请输入案件关键字";

        if (onQueryTextListener != null) {
            oSearchView.setOnQueryTextListener(onQueryTextListener);
        }
    }

    private void setCancelView(final Activity currActivity) {
        if (cancelEnable) {
            cancelBtn.setVisibility(View.VISIBLE);
            cancelLine.setVisibility(View.GONE);

            if (cancelClickListener != null) {
                cancelBtn.setOnClickListener(cancelClickListener);
            } else {
                cancelBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currActivity.finish();
                    }
                });
            }
        } else {
            cancelBtn.setVisibility(View.GONE);
            cancelBtn.setOnClickListener(null);
            cancelLine.setVisibility(View.VISIBLE);
        }
    }

    // OSearchView ==============================
    // 文本部分
    private int searchTextSize = -1; // 字体大小
    private int searchTxtColor = -1; // 字体颜色 OK
    private int searchTextHintColor = -1; // 提示颜色
    private boolean searchTextIsBold = true; //
    private String searchQueryHint = "请输入搜索关键字"; //
    private SearchView.OnQueryTextListener onQueryTextListener;

    // 图标部分
    private int searchIconRes = -1;//R.drawable.icon_search_blue; // 搜索图标
}

//    <?xml version="1.0" encoding="utf-8"?>
//<cn.android.x.view.TitleBarSearchView xmlns:android="http://schemas.android.com/apk/res/android"
//        xmlns:app="http://schemas.android.com/apk/res-auto"
//        android:id="@+id/titleBarSearchView"
//        android:layout_width="match_parent"
//        android:layout_height="@dimen/dp_70"
//        app:sub_searchIconRes="@drawable/icon_search_blue"
//        app:sub_searchQueryHint="请输入案件关键字"
//        app:sub_searchTextColor="@color/theme_color"
//        app:sub_searchTextHintColor="@color/page_sub"
//        app:sub_searchTextIsBold="true"
//        app:sub_searchTextSize="@dimen/sp_14" />

// 正常使用
//titleBarSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//        override fun onQueryTextSubmit(query: String?): Boolean {
//        XUtils.toast().show(query)
//        loadingTipView.showLoading("查询中")
//        refreshData()
//        return false
//        }
//
//        override fun onQueryTextChange(newText: String?): Boolean {
//        return false
//        }
//   }).show(this)