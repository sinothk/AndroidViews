package com.sinothk.android.views.spinner;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatSpinner;

public class EasySpinner extends AppCompatSpinner {
//    android:spinnerMode="dropdown"
//    android:spinnerMode="dialog"

    public EasySpinner(Context context) {
        super(context);
    }

    public EasySpinner(Context context, int mode) {
        super(context, mode);
    }

    public EasySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EasySpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EasySpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public EasySpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }


}
