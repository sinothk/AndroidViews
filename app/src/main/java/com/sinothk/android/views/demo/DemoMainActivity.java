package com.sinothk.android.views.demo;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sinothk.android.utils.XUtils;

/**
 * <pre>
 *  创建:  梁玉涛 2019/8/27 on 9:18
 *  项目: AndroidViewsLib
 *  描述:
 *  更新:
 * <pre>
 */
public class DemoMainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_main);

        findViewById(R.id.loadingViewDemoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        findViewById(R.id.spinnerDemoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XUtils.intent().openActivity(DemoMainActivity.this, DemoSpinnerMainActivity.class).start();
            }
        });

    }
}