package com.sinothk.android.views.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.sinothk.android.views.spinner.adapter.Person;
import com.sinothk.android.views.spinner.adapter.SpinnerArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class DemoSpinnerMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_spinner_main);

        Spinner spinner = findViewById(R.id.spinner1);

        // 建立数据源
        final List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("张三", "上海 "));
        persons.add(new Person("李四", "上海 "));
        persons.add(new Person("王五", "北京"));
        persons.add(new Person("赵六", "广州 "));
        //  建立Adapter绑定数据源
        SpinnerArrayAdapter adapter = new SpinnerArrayAdapter(this, persons);

        //绑定 Adapter到控件
        spinner.setAdapter(adapter);
        spinner.setDropDownVerticalOffset(60);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                TextView tv = (TextView) view;
                tv.setTextColor(ContextCompat.getColor(DemoSpinnerMainActivity.this, R.color.white));
                tv.setTextSize(15f);
//                String[] languages = getResources().getStringArray(R.array.languages);
                Toast.makeText(DemoSpinnerMainActivity.this, "你点击的是:" + persons.get(pos).getPersonName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });
    }
}
