package com.sinothk.android.views.demo;

import android.app.Application;

import com.sinothk.android.utils.XUtils;

public class MApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        XUtils.init(this);
    }
}
